const path = {
    sass: './sass',
    css: './css'
}

const gulp = require('gulp')
const sass = require('gulp-sass')
const autoprefixer = require('gulp-autoprefixer')
const sassLocation = path.sass + '/main.scss'
const allSassFiles = path.sass + '/**/*.scss'

gulp.task('style', () =>{
   return gulp.src(sassLocation)
        .pipe(sass())
        .pipe(autoprefixer({}))
        .pipe(gulp.dest(path.css))
})

gulp.task('watch', () => {
    return gulp.watch(allSassFiles, gulp.series('style'))
})