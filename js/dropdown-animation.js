$(document).ready(function(){
    tlanim = new TimelineMax({paused: true});
    tlanim2 = new TimelineMax({paused: true});
       

     $('.c-dropdown-mobile__dismiss').click(function(){
        tlanim
        .to($('.c-dropdown-mobile'), 0.5, {right:'-100%', ease: Power4.easeInOut})
        .set($('.c-dropdown-mobile'),{display:'none'})  
        .set($('.c-dropdown-mobile'),{right:'-100%'});  
        tlanim.play();   
    });

    $('.c-header__menu-mobile').click(function(){
    tlanim2
     .set($('.c-dropdown-mobile'),{display:'block'})
     .to($('.c-dropdown-mobile'), 0.5, {right:'0%', ease: Power4.easeInOut}) 
     .set($('.c-dropdown-mobile'),{right:'0%'})
        tlanim2.play();   
    });
});