$(document).ready(function(){
    var tlanim;

    var rating= new CountUp("rating",0,8.7,1,0.8,{
        separator: '.',
        decimal: '.'
    });
    var per1= new CountUp("per1",0,72,0,1,{
        suffix: '%'
    });
    var per2= new CountUp("per2",0,91,0,1,{
        suffix:'%'
    });
    var per3= new CountUp("per3",0,37,0,1,{
        suffix: '%'
    });
    rating.start();
    per1.start();
    per2.start();
    per3.start();
});